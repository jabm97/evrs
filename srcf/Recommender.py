from srcf import RatingsRepository as rr
from srcf import ModelGenerator as mg
import math as m



def makeRecommendations(user, repository, iimodel):
    ratingsDict = repository.ratingsDict
    moviesList = repository.items

    peliculasNoVistas = []
    for movie in moviesList:
        if user not in ratingsDict[movie]:
            peliculasNoVistas.append(movie)

    predicciones = {}

    for movieNoVista in peliculasNoVistas:
        k = 0
        sumatorioNumerador = 0
        sumatorioDenominador = 0
        for peliculaSimilar in iimodel[movieNoVista]:

            if ratingsDict[peliculaSimilar[0]].get(user):
                sumatorioNumerador += peliculaSimilar[1] * ratingsDict[peliculaSimilar[0]][user]
                sumatorioDenominador += abs(peliculaSimilar[1])
                k += 1

            if k >= 10:
                break
        if (sumatorioDenominador == 0):
            prediccion = 0
        else:
            prediccion = sumatorioNumerador / sumatorioDenominador

        predicciones[movieNoVista] = prediccion
    return predicciones


"""
def datosPeliculas(recomendaciones):
    movies = moviesRepository.moviesDict
    result = ""
    for idx, value in enumerate(recomendaciones):
        id = idx + 1
        result += (str(id) + "-" + movies[value[0]] + "->" + "%.3f" % round(value[1], 3) + "\n")
    return result
"""
