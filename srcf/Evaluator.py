from srcf import RatingsRepository as rr
from srcf import Recommender as rec
from srcf import ModelGenerator as mg

training = ["ratings_train_0.csv", "ratings_train_1.csv", "ratings_train_2.csv", "ratings_train_3.csv",
            "ratings_train_4.csv"]

test = ["ratings_test_0.csv", "ratings_test_1.csv", "ratings_test_2.csv", "ratings_test_3.csv", "ratings_test_4.csv"]
folder = "../csv/"

contador = 0
sumatoria = 0

for x in range(0, 5):
    print("Starting iteration", x)
    resultsPredicted = {}

    repoTraining = rr.RatingsRepository()
    repoTraining.loadRatings(folder + training[x])
    print("Loaded training ratings ", x)
    model = mg.generarModelo(repoTraining)

    print("Generated model item itemm ", x)
    repoTraining.loadUsers()
    repoTraining.loadItems()
    dictionaryResults = {}
    for user in repoTraining.users:
        resultsPredicted[user] = rec.makeRecommendations(user, repoTraining, model)

    print("Generated recommendations ", x)

    repoTest = rr.RatingsRepository()
    repoTest.loadRatings(folder + test[x])
    repoTest.loadUsers()
    print("Comparing results...", x)

    for user, prediction in resultsPredicted.items():

        for item in prediction:
            user = int(user)
            if repoTest.users.get(user):
                if repoTest.users[user].get(item):
                    realValue = repoTest.users[user].get(item)
                    predictionValue = resultsPredicted[user][item]
                    sumatoria += abs(predictionValue - realValue.rating)
                    contador += 1
    print("Suma total actual", sumatoria, " it ", x)
    print("Contador total actual", contador, " it ", x)

print(sumatoria / contador)
