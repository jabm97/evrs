import math

def generarModelo(ratingsRepository):
    ratingsDict = ratingsRepository.ratingsDict

    diccionarioMedias = {}

    #Calculamos la media de valoraciones de cada item
    for idItem, dictUsers in ratingsDict.items():
        suma = 0
        cont = 0
        for idUser, rating in dictUsers.items():
            suma += float(rating)
            cont += 1
        diccionarioMedias[idItem] = suma/cont

    ratingsDictIt2 = ratingsDict
    diccionarioItemItem = {}

    #Cálculo de similitud entre items mediante coeficiente de Pearson
    for key, value in ratingsDict.items():
        usuariosItem1 = value.keys()
        if key not in diccionarioItemItem:
            diccionarioItemItem[key] = {}
        for key2, value2 in ratingsDictIt2.items():
            if diccionarioItemItem.get(key2):
                if diccionarioItemItem.get(key2).get(key):
                    diccionarioItemItem[key][key2] = diccionarioItemItem[key2][key]
            if key == key2:
                pass
            else:
                diccionarioItemItem[key][key2] = 0
                usuariosItem2 = value2.keys()
                usuariosEnComun = list(set(usuariosItem1).intersection(usuariosItem2))
                numerador = 0
                denominadorA = 0
                denominadorB = 0
                for usuario in usuariosEnComun:
                    a = ratingsDict[key][usuario] - diccionarioMedias[key]
                    b = ratingsDict[key2][usuario] - diccionarioMedias[key2]
                    numerador += a*b

                    denominadorA += pow(a, 2)
                    denominadorB += pow(b, 2)
                denominadorTotal = math.sqrt(denominadorA)*math.sqrt(denominadorB)
                resultadoTotal = numerador/denominadorTotal
                if resultadoTotal >= 0:
                    diccionarioItemItem[key][key2] = resultadoTotal

    mejoresVecinos = {}
    for key, value in diccionarioItemItem.items():
        final = sorted(value.items(), key=lambda x: x[1], reverse=True)
        mejoresVecinos[key] = final[:50]

    return mejoresVecinos









