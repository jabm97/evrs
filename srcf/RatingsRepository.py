import pandas as pd


class Rating:
    def __init__(self, idUser, idItem, rating):
        self.idUser = idUser
        self.idItem = idItem
        self.rating = rating


class RatingsRepository:

    def __init__(self):
        self.ratings = []
        self.ratingsDict = {}
        self.items = []
        self.users = {}
        self.users2 = {}

    def loadRatings(self, excel_file):
        data = pd.read_csv(excel_file)
        columns = data.columns

        for index, row in data.iterrows():
            elem = Rating(row[columns[0]], row[columns[1]], row[columns[2]])
            self.ratings.append(elem)
            if elem.idItem not in self.ratingsDict:
                self.ratingsDict[elem.idItem] = {}

            if elem.idUser not in self.ratingsDict[elem.idItem]:
                self.ratingsDict[elem.idItem][elem.idUser] = 0

            self.ratingsDict[elem.idItem][elem.idUser] = elem.rating

            '''if elem.idUser not in self.users2:
                self.users2[elem.idUser] = []
            self.users2[elem.idUser].append(elem.rating)'''

    def loadItems(self):
        for rating in self.ratings:
            if rating.idItem not in self.items:
                self.items.append(rating.idItem)

    def loadUsers(self):
        for rating in self.ratings:
            if rating.idUser not in self.users:
                self.users[rating.idUser] = {}

            if rating.idItem not in self.users[rating.idUser]:
                self.users[rating.idUser][rating.idItem] = 0

            self.users[rating.idUser][rating.idItem] = rating

            if rating.idUser not in self.users2:
                self.users2[rating.idUser] = {}

            if rating.idItem not in self.users2[rating.idUser]:
                self.users2[rating.idUser][rating.idItem] = 0

            self.users2[rating.idUser][rating.idItem] = rating.rating

    def print(self):
        for elem in self.ratings:
            elem.toString()

    def countRatingsInRepository(self):
        return len(self.ratings)
