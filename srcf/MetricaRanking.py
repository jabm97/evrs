from srcf import RatingsRepository as rr
from srcf import Recommender as rec
from srcf import ModelGenerator as mg
import math as m
import operator
from statistics import mean

training = ["ratings_train_0.csv", "ratings_train_1.csv", "ratings_train_2.csv", "ratings_train_3.csv",
            "ratings_train_4.csv"]

test = ["ratings_test_0.csv", "ratings_test_1.csv", "ratings_test_2.csv", "ratings_test_3.csv", "ratings_test_4.csv"]
folder = "../csv/"

contador = 0
sumatoria = 0
resultsPredicted = {}
alfa = 3


def get10Recommendations(repoTraining, model):
    global resultsPredicted
    for user in repoTraining.users:
        resultsPredicted[user] = rec.makeRecommendations(user, repoTraining, model)
        final = sorted(resultsPredicted[user].items(), key=lambda x: x[1], reverse=True)
        resultsPredicted[user] = final[:10]

def getAllRecommendations(repoTraining,model):
    global resultsPredicted
    for user in repoTraining.users:
        resultsPredicted[user] = rec.makeRecommendations(user, repoTraining, model)
        final = sorted(resultsPredicted[user].items(), key=lambda x: x[1], reverse=True)
        resultsPredicted[user] = final

def discount(i):
    potencia = (i-1)/(alfa-1)
    return (1/(pow(2,potencia)))

def discount2(i):
    if i == 1:
        return 1
    return m.log(i,2)

# diccionario para almacenar los valores dcg de cada usuario y así luego realizar la media de todos ellos
DCGUsuarios = {}
for x in range(0, 5):
    print("Starting iteration", x)

    repoTraining = rr.RatingsRepository()
    repoTraining.loadRatings(folder + training[x])
    print("Loaded training ratings ", x)
    model = mg.generarModelo(repoTraining)

    print("Generated model item itemm ", x)
    repoTraining.loadUsers()
    repoTraining.loadItems()
    dictionaryResults = {}

    # se calculan las 10 recomendaciones para todos los usuarios
    getAllRecommendations(repoTraining, model)

    print("Generated recommendations ", x)
    # print(resultsPredicted[1])

    repoTest = rr.RatingsRepository()
    repoTest.loadRatings(folder + test[x])
    repoTest.loadUsers()
    print("Comparing results...", x)

    #2nd intento
    for user,valoraciones in repoTest.users2.items():
        print(user)
        print("items",valoraciones)
        listaOrdenada = sorted(repoTest.users2[user].items(), key=lambda x: x[1], reverse=True)[:10]
        listaOrdenadaKeys = [item[0] for item in listaOrdenada]
        print("ordenados ",listaOrdenada)
        print("inidicesordenados ",listaOrdenadaKeys)

        rev_multidict = {}
        for key, value in listaOrdenada:
            rev_multidict.setdefault(value, set()).add(key)

        # calculo del perfect rank
        perfectRank = {}
        for element in rev_multidict.items():
            itemsMismaValoracion = element[1]
            numItems = len(itemsMismaValoracion)
            suma = 0
            for i in itemsMismaValoracion:
                suma += (listaOrdenadaKeys.index(i) +1)
            #print("values",itemsMismaValoracion," len ",numItems)
            indiceElemento = suma/numItems
            for idItem in itemsMismaValoracion:
                perfectRank[idItem] = indiceElemento
                #print("iditem ",idItem," indice ",indiceElemento)

        # calculo del dcg perfecto
        user = int(user)
        dcgperfect = 0
        for element in perfectRank.items():
            rating = valoraciones[int(element[0])]
            #print("rating",rating," pos ",element[1])
            dcgperfect += rating*discount(int(element[1]))

        #print(dcgperfect)

        #dcg de nuestro sistema
        # si el usuario de test tambien existe en el modelo de prueba
        # PRUEBA 1
        dcgsystem = 0
        if user in resultsPredicted:
            predicciones = resultsPredicted[user]
            #print("predicciones",predicciones)
            for idItem in listaOrdenadaKeys:
                index = [x for x, y in enumerate(predicciones) if y[0] == int(idItem)]
                index = index[0]
                rating = (predicciones[index])[1]
                #print("index",index,"rating",rating)
                index += 1  #para que los indices empiecen en 1
                dcgsystem += rating*discount2(int(index))
        # FIN PRUEBA 1

        # PRUEBA 2
        dcgsystem2 = 0
        if user in resultsPredicted:
            predicciones = resultsPredicted[user]
            listaRecomendaciones2 = []
            for idItem in listaOrdenadaKeys:
                index = [x for x, y in enumerate(predicciones) if y[0] == int(idItem)]
                index = index[0]
                rating = (predicciones[index])[1]
                listaRecomendaciones2.append((index,rating))

            listaIndices = sorted(listaRecomendaciones2, key=lambda x: x[0], reverse=True)
            for tupla in listaIndices:
                rating = tupla[1]
                index = tupla[0] + 1
                dcgsystem2 += rating * discount2(int(index))
        # FIN PRUEBA 2


        # sustituir dcgsystem por 2 para prueba 2
        nDCG = dcgsystem2/dcgperfect
        #print("El nDCG es",nDCG)
        if user not in DCGUsuarios:
            DCGUsuarios[user] = []

        DCGUsuarios[user].append(nDCG)


#media del nDGC de todos los usuarios
contador = 0
sumatoria = 0
for usuario,ndcgs in DCGUsuarios.items():
    #print("usuario ",usuario)
    #print("ndcgs ",ndcgs)
    for ndcg in ndcgs:
        sumatoria += ndcg
        contador += 1

media = sumatoria/contador
print("EL NDCG TOTAL ES ",media)
